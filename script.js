var tl = anime.timeline({
    easing: 'easeOutExpo',
    duration: 1000
  });
  
  tl
  .add({
    targets: '.card-1',
    opacity: 1,
  })
  .add({
    targets: '.card-2',
    opacity: 1,
  })
  .add({
    targets: '.card-3',
    opacity: 1,
  })
  .add({
    targets: '.card-4',
    opacity: 1,
  });